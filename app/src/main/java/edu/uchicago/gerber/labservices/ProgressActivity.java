package edu.uchicago.gerber.labservices;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Date;

public class ProgressActivity extends Activity
        implements ServiceConnection, MyLocalService.Callback {
    private MyLocalService mService;


    private ProgressBar progressBar;
    private Button buttonService;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prog_activity);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        buttonService = (Button) findViewById(R.id.buttonService);

        buttonService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mService != null) {
                    mService.performLongRunningOperation("1463772858498");
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();


        Intent bindIntent = new Intent( ProgressActivity.this, MyLocalService.class);
        bindService(bindIntent, ProgressActivity.this, BIND_AUTO_CREATE);



    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mService != null) {
            mService.setCallback(null); // Important to avoid memory leaks
            unbindService(this);
        }
    }


    @Override
    public void onOperationProgress(int progress) {
        // TODO Update user interface with progress..

      progressBar.setProgress(progress);

    }

    @Override
    public void onOperationCompleted(Date date) {
        Toast.makeText(this, date.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceConnected(ComponentName componentName,
                                   IBinder iBinder) {
        mService = ((MyLocalService.LocalBinder) iBinder).getService();
        mService.setCallback(this);

        // Once we have a reference to the service, we can update the UI and
        // enable buttons that should otherwise be disabled.

        buttonService.setEnabled(true);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        // Disable the button as we are loosing the reference to the service.
        mService = null;
        buttonService.setEnabled(false);

    }
}
