package edu.uchicago.gerber.labservices;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BoundActivity extends AppCompatActivity {


    private RandomService randomService;
    private boolean isBound;

    private TextView textView;
    private Button buttonGet;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            RandomService.LocalBinder binder = (RandomService.LocalBinder) service;
            randomService = binder.getService();
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
                isBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bound);


        Intent intent = new Intent(this, RandomService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

         textView = (TextView) findViewById(R.id.textShow);
         buttonGet = (Button) findViewById(R.id.buttonGet);

         buttonGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(Integer.toString(randomService.getRandom()));
            }
        });



    }




}
