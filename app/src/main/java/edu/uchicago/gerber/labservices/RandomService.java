package edu.uchicago.gerber.labservices;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Random;
import java.util.Timer;

/**
 * Created by macbook on 5/25/17.
 */

public class RandomService extends Service {

    private final IBinder iBinder = new LocalBinder();
    private final Random random = new Random();

    private static Timer timer = new Timer();

    private Context ctx;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }


    public class LocalBinder extends Binder {
        RandomService getService(){
            return RandomService.this;
        }
    }

    public int getRandom(){
        return random.nextInt(1000) + 1;

    }





}
