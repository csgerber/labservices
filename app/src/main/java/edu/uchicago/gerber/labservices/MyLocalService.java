package edu.uchicago.gerber.labservices;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;

import java.util.Date;

/**
 * @author Erik Hellman
 */
public class MyLocalService extends Service {
    private static final int NOTIFICATION_ID = 1001;
    private LocalBinder mLocalBinder = new LocalBinder();
    private Callback mCallback;

    public interface Callback {
        void onOperationProgress(int progress);
        void onOperationCompleted(Date date);
    }




    public IBinder onBind(Intent intent) {
        return mLocalBinder;
    }

    public void performLongRunningOperation(String string) {
        new MyAsyncTask().execute(string);
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public class LocalBinder extends Binder {
        public MyLocalService getService() {
            return MyLocalService.this;
        }
    }



    private final class MyAsyncTask extends AsyncTask<String, Integer, Date> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          //  startForeground(NOTIFICATION_ID, buildNotification());
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if(mCallback != null && values.length > 0) {
                for (Integer value : values) {
                    mCallback.onOperationProgress(value);
                }
            }
        }

        @Override
        protected Date doInBackground(String... params) {


            int nCount = 0;
            while (nCount < 100){

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(nCount++);

            }

            return new Date(Long.parseLong(params[0]));

        }

        @Override
        protected void onPostExecute(Date date) {
            if(mCallback != null ) {
                mCallback.onOperationCompleted(date);
            }
            stopForeground(true);
        }

        @Override
        protected void onCancelled(Date date) {
            super.onCancelled(date);
            stopForeground(true);
        }
    }

    private Notification buildNotification() {
        Notification notification = null;
        // Create a notification for the service..
        return notification;
    }
}
